// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.8.0 <0.9.0;

abstract contract BaseContract {
    int public x;
    address public owner;

    constructor() {
        x = 5;
        owner = msg.sender;
    }

    function setX(int _x) public virtual;
}

interface BaseInterface {
    function setX(int _x) external;
}

contract A is BaseContract {
    int public y;

    function setX(int _x) public override {
        x = _x;
    }
}

contract B is BaseInterface {
    int public x;
    int public y;

    function setX(int _x) public override {
        x = _x;
    }
}
