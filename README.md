# Ethereum

- Three types of Ethereum nodes:
  - full node
  - light node
  - archive node
- Ethereum accounts:
  - **Externally Owned Account (EOA)**
  - **Contract Account (CA)**
- **nonce**: the transaction count of an account
- **Gas**
  - the unit that measures the amount of computational effort required to
    execute specific operations on the Ethereum network
  - measures how much work a transaction's code does
  - user that generates the transaction pays for the gas
  - for _any_ transaction there are 2 variables: `gasPrice` and `gasLimit`
  - `gasLimit` is the max gas the transaction can consume
  - gas from _all_ transactions in a block will be received by the miner that
    validates the block
  - Ethereum has a block gas limit rather than a block size
- Transactions
  - an EOA produces a transaction and a contract produces a message
  - a block is considered final and forever added to the blockchain after 12
    confirmations
- Ethereum 2.0
  - PoS
- **Proof of Stake (PoS)**
  - miners are called **validators**
  - a validator has to stake/lock an amount of ETH in a smart contract
  - To conduct a 51% attack a validator needs to stake 51% of the staked ETH
- **Sharding**
  - the blockchain is spread 64 shard chains
- Smart contract compilation
  - solidity source code is compiled into:
    - EVM bytecode, and
    - **ABI** - abstract binary interface
      - defines how this smart contract can be interacted with others
- Block time
  - 15 seconds
  - roughly every 15 seconds a new block is added
- mining

  - data + nonce = output hash
  - looking for a hash that is smaller than a target number
  - **block time**: time to find a solution
  - in reality, block time varies all the time

# Solidity & Smart Contracts

- Structure of a smart contract
  - SPDX license
  - `pragma` - enable compiler features
  - `contract`
- Variables:
  - state variables
    - **NOT** free and are permanently stored in **contract storage**
  - local variables
    - declared in functions
    - kept on stack
    - do **NOT** cost gas
    - some types by default references to the storage:
      - string
      - array
      - struct
      - mapping
    - if you want to create a string, use the `memory` keyword to limit its
      lifetime
- To change the value of a state variable:
  - use constructor
  - user a setter function
  - initialize it during declaration
- Where EVM stores data
  - storage
  - stack
  - memory
- Types of visibilities
  - `public`
  - `private`
  - `internal`
  - `external`
- if the function does not modify the storage in any way, it should be marked
  as `view` or `pure`
- calling a setter function creates a transaction that needs to be mined
- Constructor
  - executed **ONLY ONCE** when the contract is created
  - optional
- `msg.sender`
  - a global predefined/built-in variable
  - always stores the address of the owner that creates the transaction
- `constant` vs. `immutable`
  - variables cannot be modified after the contract has been constructed
  - `constant` variables have to be fixed a compile time
  - `immutable` variables can be assigned at construction time
- `int` is alias to `int256`
- `uint` is alias to `uint256`
- SafeMath
- `bytes` and `string` as arrays
  - special dynamic sized arrays
  - `bytes`
    - `bytes` is similar to `bytes1[]`
  - `string`
    - encoded in UTF-8
    - `string` does **NOT** allow `length` or indexing
- single bytes in fixed bytes arrays can **NOT** be modified
- In older code, `byte` is an alias to `bytes1`
- Dynamic arrays has three members:
  - `.length`
  - `.push`
  - `.pop`
- memory array
- fixed size arrays use less gas than dynamic arrays
- bytes and strings are reference types, **NOT** value types
- `struct`
  - by default, `struct` references storage
- `enum`
  - used to create user-defined types
  - explicitly convertible to and from integer
- Mappings
  - _all_ keys must have the same type
  - _all_ values must have the same type
  - keys can **NOT** be any of the following:
    - mapping
    - dynamic array
    - enum
    - struct
  - _always_ stored in storage, even if declared in functions
  - lookup time is constant
  - **NOT** iterable
  - we get a default value for an non-existing key
- `payable` creates a mechanism for a contract to receive ethers
- Storage vs. memory
  - if declared as `storage`, the variable is a **reference** to the state variable, which was stored on storage
- Built-in global variables
  - `msg`: info about the account that generates the transaction and also about the transaction/call
    - `msg.sender` changes every function call
      - every time when a function of the contract is called, it will have a different value
    - `msg.value`
      - eth balance (sent to this contract)
      - initially `0`
      - in `wei`
  - `gasleft()`
  - `block`
    - `block.timestamp`
    - `block.number`
    - `block.difficulty`
    - `block.gasLimit`
- `balance` is a member of any `address` variable
- Contract address
  - any contract has its own _unique_ address, generated at deployment time
  - calculated based on:
    - address of the creator of the contract
    - number of transactions of that account (**nonce**)
  - 2 types of addresses:
    - **payable**
    - **plain** (non-payable)
  - Address is a variable type and has the following members:
    - `balance`
    - `transfer()` - recommended for most use cases
      - safest
    - `send()`
      - low-level version of `transfer()`
    - `call()`
    - `callcode()`
    - `delegatecall()`
- A contract receives ETH in 2 ways:
  - simply send ETH to the contract address by/from an EOA account
    - the contract needs at least one of the functions:
      - `receive()`
      - `fallback()`
  - call a payable function and send ETH with that transaction
- Visibility
  - `public`
    - default for functions
    - a getter is automatically created for public variables
  - `private`
    - subset of `internal`
    - can _only_ be called within the same contract
    - can _only_ be accessed via a getter function
  - `internal`
    - can be accessed in the contract they are defined in as well as derived contracts
    - default for state variables
  - `external`
    - functions are contract interfaces
    - can be accessed **only from other contracts** or by EOA accounts using transactions
    - automatically public
    - **NOT** available for state variables
    - **more efficient** than `public` functions in terms of gas consumption
- Function modifiers

```solidity

modifier someModifier() {
    require();
    _; // function body is inserted here
}
```

- You cannot return in a `public` function
  - you _could_, but it will **NOT** actually return the value in the `return` statement
  - because if a function is a transaction, it returns the transaction hash

## The Auction Project

- the **withdraw** pattern
  - do **NOT** proactively send back the funds to users, unless they explicitly request it
  - prevents **re-entrance** attacks
- how to scale?
  - we do **NOT** send the contract's bytecode to user
    - subject to users' changes
  - we actually create a contract that deploys new contract instances
- Contract that deploys another contract
- Solidity Events
  - each Ethereum transaction has attached to it a receipt which contains log entries
  - called **events** in Solidity
  - called **logs** in EVM
  - events allow JavaScript callback functions that listen for them in the user interface to update the interface
    accordingly
  - **NOT** accessible from within the contracts
  - **ONLY** can be accessed by external actors such as JavaScript
  - events are inheritable
    - you declare an event in an interface or a base contract
    - in derived contracts, you do **NOT** declare them again
    - you just emit it
  - It's good practice for a contract to emit events
- Inheritance
  - all functions are **virtual**
    - the most derived function is called, except when the contract name is explicitly given
  - when deploying a derived contract the base contract's constructor is automatically called
  - `is`
- an `abstract` contract cannot be deployed on the blockchain
  - can only be inherited
- a function without implementation must be marked as `virtual`
- a contract with at least one `virtual` function must be marked as `abstract`
- if a derived contract doesn't implement all inherited `virtual` functions, it should also be marked as `abstract`
- when overriding a `virtual` function in a derived contract, you must explicitly use `override`
- It's **NOT** allowed to override a state variable that's already declared in the base contract
- Interfaces
  - similar to `abstract` contracts
  - but cannot have _any_ functions implemented
- **ERC (Ethereum Request for Comments)**
  - a proposal to define standards and practices
- **EIP (Ethereum Improvement Proposal)**
  - makes changes to the actual code
- ERC20
  - `allowance`
    - the token owner gives another address (often a contract) approval to transfer up to a number of tokens
- IPFS and Swarm are two options
- IPFS - InterPlanetary File System
  - **content hashing**
    - instead of **location hashing**
  - pinning
    - pinning services:
      - infura.io
      -

# Front-end Project

## Testing

- Use `mocha`
- make sure to name the test folder as `/test`
-
