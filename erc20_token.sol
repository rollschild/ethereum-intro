// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.8.0 <0.9.0;

interface ERC20Interface {
    // only the first three functions are necessary
    function totalSupply() external view returns (uint);
    function balanceOf(address tokenOwner) external view returns (uint balance);
    function transfer(address to, uint tokens) external returns (bool success);

    function allowance(address tokenOwner, address spender) external view returns (uint remaining);
    function approve(address spender, uint tokens) external returns (bool success);
    function transferFrom(address from, address to, uint tokens) external returns (bool success);

    event Transfer(address from, address to, uint tokens);
    event Approval(address indexed tokenOwner, address indexed spender, uint tokens); 
}

contract ChameleonToken is ERC20Interface {
    string public name = "Chameleon";
    string public symbol = "CMLN";
    uint public decimals = 0; // 18 is the most used
    uint public override totalSupply; // implicitly created a getter function because it's public

    address public founder;
    mapping(address => uint) public balances;

    // approver => approved => approved amount
    mapping(address => mapping(address => uint)) allowed;

    constructor() {
        totalSupply = 1e6;
        founder = msg.sender;
        balances[founder] = totalSupply;
    }

    function balanceOf(address tokenOwner) public view override returns (uint balance) {
        return balances[tokenOwner];
    }

    function transfer(address to, uint tokens) public override returns (bool success) {
        require(balances[msg.sender] >= tokens);
        balances[to] += tokens;
        balances[msg.sender] -= tokens;

        emit Transfer(msg.sender, to, tokens);

        return true;
    }

    function allowance(address tokenOwner, address spender) public view override returns (uint) {
        return allowed[tokenOwner][spender];
    }

    function approve(address spender, uint tokens) public override returns (bool success) {
        require(balances[msg.sender] >= tokens, "You do not have enough funds!");
        require(tokens > 0);

        allowed[msg.sender][spender] = tokens;

        emit Approval(msg.sender, spender, tokens);

        return true;
    }

    function transferFrom(address from, address to, uint tokens) external override returns (bool success) {
        require(allowed[from][msg.sender] >= tokens);
        require(balances[from] >= tokens);
        balances[from] -= tokens;
        allowed[from][msg.sender] -= tokens;

        balances[to] += tokens;

        emit Transfer(from, to, tokens);

        return true;
    }
}
