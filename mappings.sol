// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.14;

contract Auction {
    mapping(address => uint) public bids;

    function bid() payable public {

        // msg.sender is the address that calls this function in a transaction
        // msg.value is the wei that is sent when calling this function
        bids[msg.sender] = msg.value;
    }
}
