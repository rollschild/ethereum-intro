// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.14;

contract A {
    string[] public cities = ["Washington, D.C.", "New York City"];

    function f_memory() view public {
        string[] memory s1 = cities;
        s1[0] = "Beijing";
    }

    function f_storage() public {
        // s1 here is a reference to the state variable
        string[] storage s1 = cities;
        s1[0] = "Atlanta";
    }
}
