// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.14;

contract FixedSizeArrays {
    uint[3] public numbers = [3, 2, 1]; // array literal

    bytes1 public b1;
    bytes2 public b2;
    bytes3 public b3;
    // ...up to bytes32

    function setElement(uint index, uint value) public {
        numbers[index] = value;
    }

    function getLength() public view returns (uint) {
        return numbers.length;
    }

    function setBytesArray() public {
        b1 = "a";
        b2 = "ab";
        b3 = "ac";
    }
}

contract DynamicArrays {
    uint[] public numbers;

    function getLength() public view returns (uint) {
        return numbers.length;
    }

    function addElement(uint item) public {
        numbers.push(item);
    }

    function getElement(uint index) public view returns(uint) {
        if (index < numbers.length) {
            return numbers[index];
        }
        return 0;
    }

    function popElement() public {
        numbers.pop();
    }

    function newMemoryArray() public {
        // cannot be resized
        // push and pop are not available
        uint[] memory y = new uint[](4);

        y[0] = 1;
        y[1] = 2;
        y[2] = 3;
        y[3] = 4;
        numbers = y;
    }
}

