// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.8.0 <0.9.0;

contract CrowdFunding {
    mapping(address => uint) public contributors;
    address public admin;
    uint public numberOfContributors;
    uint public goal;
    uint public minContribution;
    uint public deadline;
    uint public raisedAmount;

    event ContributeEvent(address _sender, uint _value);
    event CreateSpendingRquestEvent(string _description, address _recipient, uint _value);
    event MakePaymentEvent(address _recipient, uint _value);

    struct SpendingRequest {
        string description;
        address payable recipient;
        uint value;
        bool completed;
        uint numberOfVoters;
        mapping(address => bool) voters;
    }

    // requests cannot be stored in dynamic array
    // because in latest versions of Solidity, assignments to arrays in storage do not work
    // ...if they contain mappings
    mapping(uint => SpendingRequest) public spendingRequests;
    uint public numberOfRequests;

    modifier onlyAdmin() {
        require(msg.sender == admin, "Only Admin can call this function!");
        _;
    }

    function createSpendingRequest(string memory _description, address payable _recipient, uint _value) public onlyAdmin {
        // the struct contains a nested mapping and must be stored in storage
        SpendingRequest storage request = spendingRequests[numberOfRequests];
        numberOfRequests++;

        request.description = _description;
        request.recipient = _recipient;
        request.value = _value;
        request.completed = false;
        request.numberOfVoters = 0;

        emit CreateSpendingRquestEvent(_description, _recipient, _value);
    }

    function voteSpendingRequest(uint _requestNumber) public {
        require(contributors[msg.sender] > 0, "You must be a contributor to vote!");
        SpendingRequest storage request = spendingRequests[_requestNumber];

        // current voter has not voted yet
        require(request.voters[msg.sender] == false, "You have already voted!");

        request.voters[msg.sender] = true;
        request.numberOfVoters++;
    }

    function makePayment(uint _requestNumber) public onlyAdmin {
        require(raisedAmount >= goal, "Not enough funds raised!");
        SpendingRequest storage request = spendingRequests[_requestNumber];
        require(request.completed == false, "The spending request has been completed!");
        require(request.numberOfVoters > (numberOfContributors / 2), "Not enough voters have voted!");

        request.recipient.transfer(request.value);
        request.completed = true;

        emit MakePaymentEvent(request.recipient, request.value);
    }

    constructor(uint _goal, uint _deadline) {
        goal = _goal;
        deadline = block.timestamp + _deadline;
        minContribution = 100; // in wei
        admin = msg.sender;
    }

    function contribute() public payable {
        require(block.timestamp < deadline, "Deadline has passed!");
        require(msg.value >= minContribution, "Minimum contribution not met!");

        if (contributors[msg.sender] == 0) {
            numberOfContributors++;
        }

        contributors[msg.sender] += msg.value;
        raisedAmount += msg.value;

        emit ContributeEvent(msg.sender, msg.value);
    }

    // to send ETH directly to the contract's address, a `receive` function needs to be defined
    receive() payable external {
        contribute();
    }

    function getBalance() public view returns (uint) {
        return address(this).balance;
    }

    // if goal is not met after the deadline, users can request a refund
    function getRefund() public {
        require(block.timestamp > deadline && raisedAmount < goal);

        // only contributor can request a refund
        require(contributors[msg.sender] > 0);

        address payable recipient = payable(msg.sender);
        uint value = contributors[msg.sender];
        recipient.transfer(value);

        contributors[msg.sender] = 0;

    }
}
