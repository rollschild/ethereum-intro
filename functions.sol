// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.14;

contract Property {
    // state
    int public price;
    string public location;
    address immutable public owner;
    int constant area = 96;

    constructor(int _price, string memory _location) {
        price = _price;
        location = _location;
        owner = msg.sender;
    }
}
