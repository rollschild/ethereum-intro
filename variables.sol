// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.14;

contract Property {
    // state
    int public value;
    string constant public location = "Washington, D.C.";


    function setValue(int _value) public {
        value = _value;
    }

    function f1() public pure returns(int) {
        int x = 42;
        x -= 6;
        return x;
    }

    function f2() public returns(string memory) {
        string memory s1 = "some string";
        return s1;
    }
}
