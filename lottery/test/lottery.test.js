const assert = require("assert");
const ganache = require("ganache");
const { beforeEach } = require("mocha");
const Web3 = require("web3");
const web3 = new Web3(ganache.provider());
const { abi, evm } = require("../compile");

let accounts;
let lottery;

beforeEach(async () => {
  // get a list of all accounts
  // everything in web3 is async

  accounts = await web3.eth.getAccounts();

  // use one of the accounts to deploy
  lottery = await new web3.eth.Contract(abi)
    .deploy({ data: evm.bytecode.object })
    .send({
      gas: "1000000",
      from: accounts[0],
    });
});

describe("Lottery", () => {
  it("deploys a contract", () => {
    assert.ok(lottery.options.address);
  });

  it("allows one account to enter the lottery", async () => {
    // await lottery.methods.receive().send({
    //   from: accounts[0],
    //   value: web3.utils.toWei("0.1", "ether"),
    // });

    // REMEBER to use `await` here!
    // EVERY transaction is async!
    await web3.eth.sendTransaction({
      from: accounts[0],
      to: lottery.options.address,
      value: web3.utils.toWei("0.1", "ether"),
    });

    const players = await lottery.methods.getPlayers().call({
      from: accounts[0],
    });

    assert.equal(accounts[0], players[0]);
    assert.equal(1, players.length);
  });

  it("allows multiple accounts to enter the lottery", async () => {
    // REMEBER to use `await` here!
    // EVERY transaction is async!
    await web3.eth.sendTransaction({
      from: accounts[0],
      to: lottery.options.address,
      value: web3.utils.toWei("0.1", "ether"),
    });
    await web3.eth.sendTransaction({
      from: accounts[1],
      to: lottery.options.address,
      value: web3.utils.toWei("0.1", "ether"),
    });
    await web3.eth.sendTransaction({
      from: accounts[2],
      to: lottery.options.address,
      value: web3.utils.toWei("0.1", "ether"),
    });

    const players = await lottery.methods.getPlayers().call({
      from: accounts[0],
    });

    assert.equal(accounts[0], players[0]);
    assert.equal(accounts[1], players[1]);
    assert.equal(accounts[2], players[2]);
    assert.equal(3, players.length);
  });

  it("requires exactly 0.1 ether to enter", async () => {
    try {
      await web3.eth.sendTransaction({
        from: accounts[0],
        to: lottery.options.address,
        value: web3.utils.toWei("0.101", "ether"),
      });
      assert(false);
    } catch (error) {
      assert(error);
    }
  });

  it("only manager can call pickWinner", async () => {
    await web3.eth.sendTransaction({
      from: accounts[0],
      to: lottery.options.address,
      value: web3.utils.toWei("0.1", "ether"),
    });
    await web3.eth.sendTransaction({
      from: accounts[1],
      to: lottery.options.address,
      value: web3.utils.toWei("0.1", "ether"),
    });
    await web3.eth.sendTransaction({
      from: accounts[2],
      to: lottery.options.address,
      value: web3.utils.toWei("0.1", "ether"),
    });

    try {
      await lottery.methods.pickWinner().send({
        from: accounts[1],
      });
      assert(false);
    } catch (error) {
      assert(error);
    }
  });

  it("sends money to the winner and resets the players array", async () => {
    await web3.eth.sendTransaction({
      from: accounts[0],
      to: lottery.options.address,
      value: web3.utils.toWei("0.1", "ether"),
    });
    await web3.eth.sendTransaction({
      from: accounts[1],
      to: lottery.options.address,
      value: web3.utils.toWei("0.1", "ether"),
    });
    await web3.eth.sendTransaction({
      from: accounts[2],
      to: lottery.options.address,
      value: web3.utils.toWei("0.1", "ether"),
    });

    const initialBalance = await web3.eth.getBalance(accounts[0]);

    await lottery.methods.pickWinner().send({
      from: accounts[0],
    });

    const finalBalance = await web3.eth.getBalance(accounts[0]);

    const difference = finalBalance - initialBalance;

    // some gas consumed
    assert(Number(difference) > web3.utils.toWei("0.08", "ether"));
  });
});
