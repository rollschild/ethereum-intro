// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

contract A {
    int public x = 10;
    int y = 20; // internal

    function get_y() public view returns (int) {
        return y;
    }


    // cannot be called in remix because remix is external
    function f1() private view returns(int) {
        return x;
    }

    function f2() public view returns (int) {
        return f1();
    }

    function f3() internal view returns(int) {
        return x;
    }

    function f4() external view returns(int) {
        return x;
    }
}

// declaring a derived contract
contract B is A {
    int public xx = f3();
    // int public yy = f1();
}

// contract C deploys contract A
contract C {
    A public contract_a  = new A();
    int public xx = contract_a.f4(); // note f4 is external
}
