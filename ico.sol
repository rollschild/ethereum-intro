// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.8.0 <0.9.0;

interface ERC20Interface {
    // only the first three functions are necessary
    function totalSupply() external view returns (uint);
    function balanceOf(address tokenOwner) external view returns (uint balance);
    function transfer(address to, uint tokens) external returns (bool success);

    function allowance(address tokenOwner, address spender) external view returns (uint remaining);
    function approve(address spender, uint tokens) external returns (bool success);
    function transferFrom(address from, address to, uint tokens) external returns (bool success);

    event Transfer(address from, address to, uint tokens);
    event Approval(address indexed tokenOwner, address indexed spender, uint tokens); 
}

contract ChameleonToken is ERC20Interface {
    string public name = "Chameleon";
    string public symbol = "CMLN";
    uint public decimals = 0; // 18 is the most used
    uint public override totalSupply; // implicitly created a getter function because it's public

    address public founder;
    mapping(address => uint) public balances;

    // approver => approved => approved amount
    mapping(address => mapping(address => uint)) allowed;

    constructor() {
        totalSupply = 1e6;
        founder = msg.sender;
        balances[founder] = totalSupply;
    }

    function balanceOf(address tokenOwner) public view override returns (uint balance) {
        return balances[tokenOwner];
    }

    function transfer(address to, uint tokens) public virtual override returns (bool success) {
        require(balances[msg.sender] >= tokens);
        balances[to] += tokens;
        balances[msg.sender] -= tokens;

        emit Transfer(msg.sender, to, tokens);

        return true;
    }

    function allowance(address tokenOwner, address spender) public view override returns (uint) {
        return allowed[tokenOwner][spender];
    }

    function approve(address spender, uint tokens) public override returns (bool success) {
        require(balances[msg.sender] >= tokens, "You do not have enough funds!");
        require(tokens > 0);

        allowed[msg.sender][spender] = tokens;

        emit Approval(msg.sender, spender, tokens);

        return true;
    }

    function transferFrom(address from, address to, uint tokens) public virtual override returns (bool success) {
        require(allowed[from][msg.sender] >= tokens);
        require(balances[from] >= tokens);
        balances[from] -= tokens;
        allowed[from][msg.sender] -= tokens;

        balances[to] += tokens;

        emit Transfer(from, to, tokens);

        return true;
    }
}

contract ChameleonTokenICO is ChameleonToken {
    address public admin;
    address payable depositAddress;
    uint tokenPrice = 0.001 ether;
    uint public hardCap = 300 ether;
    uint public raisedAmount;
    uint public saleStart = block.timestamp;
    uint public saleEnd = block.timestamp + 604800;
    uint public tokenTradeStart = saleEnd + 604800;
    uint public maxInvestment = 5 ether;
    uint public minInvestment = 0.1 ether;

    enum State {
        beforeStart,
        running,
        afterEnd,
        halted
    }

    State public icoState;

    constructor(address payable _depositAddress) {
        depositAddress = _depositAddress;
        admin = msg.sender;
        icoState = State.beforeStart;
    }

    modifier onlyAdmin() {
        require(msg.sender == admin);
        _;
    }

    function halt() public onlyAdmin {
        icoState = State.halted;
    }

    function resume() public onlyAdmin {
        icoState = State.running;
    }

    function changeDepositAddress(address payable newDepositAddress) public onlyAdmin {
        depositAddress = newDepositAddress;
    }

    function getCurrentState() public view returns (State) {
        if (icoState == State.halted) {
            return State.halted;
        } else if (block.timestamp < saleStart) {
            return State.beforeStart;
        } else if (block.timestamp >= saleStart && block.timestamp <= saleStart) {
            return State.running;
        } else {
            return State.afterEnd;
        }
    }

    event Invest(address investor, uint value, uint tokens);

    function invest() public payable returns (bool) {
        icoState = getCurrentState();
        require(icoState == State.running);
        require(msg.value >= minInvestment && msg.value <= maxInvestment);
        require(raisedAmount + msg.value <= hardCap);

        raisedAmount += msg.value;

        uint tokens = msg.value / tokenPrice;

        balances[msg.sender] += tokens;
        balances[founder] -= tokens;

        depositAddress.transfer(msg.value);

        emit Invest(msg.sender, msg.value, tokens);

        return true;
    }

    // if directly sending ETH to the contract's address
    receive() payable external {
        invest();
    }

    function transfer(address to, uint tokens) public override returns (bool success) {
        require(block.timestamp > tokenTradeStart);
        super.transfer(to, tokens);

        return true;
    }

    function transferFrom(address from, address to, uint tokens) public override returns (bool success) {
        require(block.timestamp > tokenTradeStart);
        super.transferFrom(from, to, tokens);

        return true;
    }

    // burn any unsold tokens, as a good practice
    function burn() public returns (bool) {
        // anyone can burn
        icoState = getCurrentState();
        require(icoState == State.afterEnd);
        balances[founder] = 0;

        return true;
    }
}
