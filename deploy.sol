// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.8.0 <0.9.0;

contract ToBeDeployed {
    address public owner; // whoever deployed this contract, ie. whoever called `deploy()`
    // not necessarily the owner of Creator contract
    constructor(address caller) {
        owner = caller;
    }
}

contract Creator {
    address public owner;
    ToBeDeployed[] public deployed;

    constructor() {
        owner = msg.sender;
    }

    function deploy() public {
        ToBeDeployed deployedAddress = new ToBeDeployed(msg.sender);
        deployed.push(deployedAddress);
    }
}
