// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.14;

contract BytesAndStrings {
    bytes public b1 = "abcd";
    string public s1 = "abcd";

    function addElement() public {
        b1.push("e");

        // you cannot do this:
        // s1.push("e");
    }

    function getElement(uint index) public view returns (bytes1) {
        return b1[index];
    }

    function getLength() public view returns (uint) {
        return b1.length;
    }
}
